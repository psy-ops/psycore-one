---
layout: single-overview
description: An overview of the instructions for eliciting qualitative data
#header:
#  caption: null
#  caption_url: null
#  image: novelist.jpg
title: Eliciting Qualitative Data
url: /qualitative-data/
instructiontype: instructions to elicit qualitative data
instructiontypeCapitalized: Instructions to elicit qualitative data
dctField: aspect_dev
psycoreImage: dct_aspect.svg
---

This overview shows the instructions for eliciting qualitative data for all constructs in this repository.
