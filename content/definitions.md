---
layout: single-overview
description: An overview of construct definitions
#header:
#  caption: null
#  caption_url: null
#  image: novelist.jpg
title: Construct Definitions
url: /definitions/
instructiontype: construct definitions
instructiontypeCapitalized: Construct definitions
dctField: definition
psycoreImage: dct_definition.svg
---

This overview shows the construct definitions for all constructs in this repository.
