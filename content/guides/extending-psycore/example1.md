---
date: "2019-05-05T00:00:00+01:00"
draft: false
linktitle: Adding DCTs
menu:
  example:
    parent: Extending PsyCoRe.one
    weight: 1
title: Extending PsyCoRe.one by adding DCTs
toc: true
type: docs
weight: 1
---

An explanation of how to add .dct.yaml files using a pull request to the [`data/dctspecs` directory in the PsyCoRe.one GitLab repository](https://gitlab.com/psy-ops/psycore-one/-/tree/master/data/dctspecs).

