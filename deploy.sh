echo
echo ----------
echo Date: $(date)

### Go to directory with cloned git repo
cd ~/pulled_repo

git log -1 --pretty=format:"Last commit: %an, %ar: %s (%h)"

### Set the path so Hugo can be found
#PATH=$PATH:/var/www/vhosts/psycore.one/.local/share/Hugo/0.74.3

### Delete old 'public' directory if it exists
rm -rf public

echo Current path and $PATH contents:
pwd
echo $PATH
echo Calling blogdown

#R -e "options(blogdown.hugo.version = '0.74.3'); blogdown::build_site();"
/usr/local/bin/R -e "blogdown::build_site();"

echo Finished blogdown

### Delete all contents in public HTML directory
echo Deleting everything in ~/public_html
rm -rf ~/public_html/*.*
rm -rf ~/public_html/*
rm -f ~/public_html/.htaccess
echo Finished deleting everything in ~/public_html

### Copy website
echo Copying website from ~/pulled_repo/public to ~/public_html
cp -RT public ~/public_html

echo Done copying website, copying .htaccess

### Copy .htaccess
cp .htaccess ~/public_html

echo Done with everything.

echo ----------
