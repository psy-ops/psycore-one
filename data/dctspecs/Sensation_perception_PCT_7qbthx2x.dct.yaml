---
dct:
  version: 0.2.6
  id: Sensation_perception_PCT_7qbthx2x
  label: Sensation perception
  date: '2023-09-05'
  dct_version: '1'
  ancestry: ''
  retires: ''
  definition:
    definition: 'A sensation perception is the second-order signal and it is a combination
      of numerous intensity signals. Powers (2005, p. 100) calls sensations "the weighted
      sum of first-order signals" (first order signals are intensities; See also Intensity_perception_PCT_7qbth5l1).
      As such, a sensation cannot itself correspond one-to-one to an aspect of the
      outside world the way intensities (see Intensity_perception_PCT_7qbth5l1) do.
      Rather it is a derivative quantity. It is a quantity that cannot be found in
      the outside world. For instance, we perceive certain wavelengths of light, as
      analogues of those bands of wavelengths. We perceive colours therefore in a
      certain way. An instrument can measure the physical basis of, say, what we all
      call "blue", but it can''t say whether we all subjectively experience "blue"
      is the same way. Instruments can detect wavelengths, but no one other than us
      knows what the colour looks like for us (Runkel, 2003).  '
  measure_dev:
    instruction: "To measure sensation perception, it needs to be tested if the participant
      is sensitive for a certain sound, smell, taste, touch, quality or sensorimotor
      perception. At this level, we measure if the participant can perceive something
      with their senses. To measure the perception of sensations, the following question
      can be asked:  \n‘Do you sense something?’, ‘Do you smell/hear/see/feel something?’ "
  measure_code:
    instruction: 'Instruments measure sensations, if they ask whether a person senses
      a sound, colour, smell, etc. This is a singular sensation, without labels or
      without configuration (that would make it a higher order perception).  '
  aspect_dev:
    instruction: "To elicit expressions about sensations, the participant needs to
      be in the position to experience some kind of sensation with their senses, whether
      it be sound, taste, smell, quality etc.  \nOne could offer a certain sound,
      smell, vision or another quality and the participant can indicate whether they
      sense something. \n"
  aspect_code:
    instruction: "Expressions about sensations are about experiences of senses. For
      instance: \nI can smell something. I can hear something. I can feel something.
      I can view something. (Without the higher order labelling or complexity of configurations).
      \ \nAnother example of expression occurs when we see a painter mixing paints
      to get the desired colour; the intensities of different reflected wavelengths
      are being combined to produce the desired sensation (colour). Another example
      of an expression about control of sensation perception is tuning an instrument.
      Pitch is a perception that is derived from the intensities of the different
      frequency components of a sound.\n"
  comments: ''
  rel: ~

---

