---
dct:
  version: 0.2.6
  id: Sequence_perception_PCT_7qbtl7sp
  label: Sequence perception
  date: '2023-09-05'
  dct_version: '1'
  ancestry: ''
  retires: ''
  definition:
    definition: "Sequence perception, the eighth level of perception in Powers' (1998)
      hierarchy, is how perceptions of underlying levels are ordered in time, space
      or process. A sequence can exist from different orders of underlying perceptions.\nUsing
      sequence control, it is possible to start later in the sequence, or stop before
      the end, but it still remains (part of) a sequence. Only when you change the
      order of the sequential components, you will no longer perceive (control at
      the level of) the sequence (Runkel, 2003). \nWe experience this level as predictability
      and expectations. It allows us to orientate in time and space. At this level
      of control, no decisions are made. Decisions or choices happen at a higher level.
      At the level of sequences, we follow along a path, we go along with the flow
      of things. At the level of sequences, the references are predictions and expectations.
      This means that control at this level is perceived as predictability: the next
      step is obvious. Control at this level is lost when the sequence breaks, for
      instance when you are walking a trail walk, you missed a sign and find yourself
      lost. Control can be regained by (re)starting somewhere along the sequence,
      for instance where you last saw a trail sign (De Hullu, 2023). "
  measure_dev:
    instruction: 'To measure sequence perception, you first need to determine a sequence
      to be measured. For example a melody, a recipe, a goal that is reached through
      fixed steps. Next, the participant is asked to write all of the sequential steps
      in a list. These steps cannot contain choice points that have not yet been decided
      (if/then statements; program level).  '
  measure_code:
    instruction: 'An instrument measures at the level of sequences, when it asks a
      participant to order items. The sequence is always in this same order, so to
      determine a sequence, a participant can be asked to explain every single step
      that is needed to complete a certain sequence.  '
  aspect_dev:
    instruction: 'To elicit construct content, a participant can be given a unfinished
      sequence. For instance: give the participant a paper with ''1, 2, 3, _, _''
      on it. When a person is perceiving a sequence, they write down the rest of the
      sequence. Another example is to give a person a sentence, where the words are
      not in the right order. A person that perceives or notices a (disturbed) sequence,
      tries to find the right sequence and possibly correct it.  '
  aspect_code:
    instruction: "Expressions of sequences follow fixed steps. If a participant counts
      to 10, they will start with 1, 2, 3 and count all the way to 10. This means
      they perceive a sequence. The order is fixed and does not contain choice-points.
      \n \nExample: 'When I take a drive in my car, I walk towards the car, open the
      car door, take a seat, start the engine, shift to first gear and step on the
      throttle'. It makes no sense to first step on the throttle, when the car is
      still off. \n"
  comments: ''
  rel: ~

---

